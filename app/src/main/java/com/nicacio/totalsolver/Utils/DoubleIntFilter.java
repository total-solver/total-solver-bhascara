package com.nicacio.totalsolver.Utils;

public class DoubleIntFilter {
    private final static double epsilon = 1E-10;

    public static boolean isInteger(final double d) {
        return Math.abs(Math.floor(d) - d) < epsilon;
    }

    public static String result(final double number) {
        if (DoubleIntFilter.isInteger(number)) {
            int value = (int) number;
            if (value < 0) {
                return "(" + value + ")";
            } else {
                return String.valueOf(value);
            }
        } else {
            if (number < 0) {
                return "(" + number + ")";
            } else {
                return String.valueOf(number);
            }
        }
    }
}
