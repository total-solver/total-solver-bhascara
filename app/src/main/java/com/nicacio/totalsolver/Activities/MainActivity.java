package com.nicacio.totalsolver.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.nicacio.totalsolver.R;
import com.nicacio.totalsolver.Utils.DoubleIntFilter;

import io.github.kexanie.library.MathView;

public class MainActivity extends AppCompatActivity {

    private EditText a;
    private EditText b;
    private EditText c;
    private MathView formula_with_user_inputs;
    private String tex_formula;
    private TextView adv;
    private TextView step_1;
    private MathView step_1_quation_demo;
    private MathView step_1_equation;
    private String step_1_text;
    private MathView step_1_equation_2;
    private String step_1_text_2;
    private MathView step_1_equation_3;
    private String step_1_text_3;
    private TextView step_2;
    private MathView step_2_equation;
    private MathView step_2_equation_1;
    private String step_2_text_1;
    private MathView step_2_equation_2;
    private String step_2_text_2;
    private MathView step_2_equation_3;
    private String step_2_text_3;
    private MathView step_2_equation_4;
    private TextView solution_description;
    private String step_2_text_4;
    private MathView solution;
    private String solution_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a = findViewById(R.id.editText_1);
        b = findViewById(R.id.editText_2);
        c = findViewById(R.id.editText_3);
        formula_with_user_inputs = findViewById(R.id.formula_with_user_inputs);
        adv = findViewById(R.id.adv);
        step_1 = findViewById(R.id.step_1);
        step_1_quation_demo = findViewById(R.id.formula_two);
        step_1_equation = findViewById(R.id.step_1_equation);
        step_1_equation_2 = findViewById(R.id.step_1_equation_2);
        step_1_equation_3 = findViewById(R.id.step_1_equation_3);
        step_2 = findViewById(R.id.step_2);
        step_2_equation = findViewById(R.id.formula_three);
        step_2_equation_1 = findViewById(R.id.step_2_equation_1);
        step_2_equation_2 = findViewById(R.id.step_2_equation_2);
        step_2_equation_3 = findViewById(R.id.step_2_equation_3);
        step_2_equation_4 = findViewById(R.id.step_2_equation_4);
        solution_description = findViewById(R.id.solution_description);
        solution = findViewById(R.id.solution);

    }

    public void calculate(View view) {
        bhascaraCalculations(Double.valueOf(a.getText().toString()), Double.valueOf(b.getText().toString()), Double.valueOf(c.getText().toString()));
    }

    public void bhascaraCalculations(double a, double b, double c) {
        double delta = b*b - 4*a*c;
        double x1;
        double x2;
        String aa = DoubleIntFilter.result(a);
        String bb = DoubleIntFilter.result(b);
        String cc = DoubleIntFilter.result(c);
        if (delta > 0.0) {
            x1 = (-b + Math.sqrt(delta)) / (2 * a);
            x2 = (-b - Math.sqrt(delta)) / (2 * a);
            tex_formula =  "$$" + aa + "x^2 + " + bb + "x + " + cc + " = 0" + "$$";
            adv.setText(R.string.Adv_Reals);
            step_1_text = "$$" + "\\" + "Delta" + "\\" + " = " + bb + "^2 - " + "4" + "\\" + "cdot" + aa + "\\" + "cdot" + cc + "$$";
            String bxb = DoubleIntFilter.result(b * b);
            String _4ac = DoubleIntFilter.result(4 * a * c);
            step_1_text_2 = "$$" + "\\" + "Delta" + "\\" + " = " + bxb + " - " + _4ac + "$$";
            String deltaResult = DoubleIntFilter.result(delta);
            step_1_text_3 = "$$" + "\\" + "Delta" + "\\" + " = " + "\\" + "bold" + "{" + deltaResult + "}" + "$$";
            step_2_text_1 = "$$" + "x_1 = " + "{-" + bb + " + " + "\\" + "sqrt{" + bb + "^2 - " + "4" + "\\" + "cdot" + aa +
                    "\\" + "cdot" + cc + "}" + "\\" + "over 2" + "\\" + "cdot" + aa + "}" +"$$";
            String fraction_1 = DoubleIntFilter.result(-b + Math.sqrt(delta));
            step_2_text_2 = "$$" + "x_1 = " + "{-" + bb + " + " + DoubleIntFilter.result(Math.sqrt(delta)) + "\\" + "over 2" + "\\" + "cdot" + aa + "}" + " = " +
                    "{" +  fraction_1 + "\\" + "over 2" + "\\" + "cdot" + aa + "}" + " = " + "\\" + "bold" + "{" + DoubleIntFilter.result(x1) + "}" + "$$";
            step_2_text_3 = "$$" + "x_2 = " + "{-" + bb + " - " + "\\" + "sqrt{" + bb + "^2 - " + "4" + "\\" + "cdot" + aa +
                    "\\" + "cdot" + cc + "}" + "\\" + "over 2" + "\\" + "cdot" + aa + "}" +"$$";
            String fraction_2 = DoubleIntFilter.result(-b - Math.sqrt(delta));
            step_2_text_4 = "$$" + "x_2 = " + "{-" + bb + " - " + DoubleIntFilter.result(Math.sqrt(delta)) + "\\" + "over 2" + "\\" + "cdot" + aa + "}" + " = " +
                    "{" +  fraction_2 + "\\" + "over 2" + "\\" + "cdot" + aa + "}" + " = " + "\\" + "bold" + "{" + DoubleIntFilter.result(x2) + "}" + "$$";
            solution_text = "$$" + "\\" + "large" + "{" + "S" + "}" + " = " + "\\" + "Big" + "\\" + "{" + " " + DoubleIntFilter.result(x1) + ", " +
                    DoubleIntFilter.result(x2) + "\\" + "Big" + "\\" + "}" + "$$";
        } else if (delta < 0.0) {
            //duas raizes complexas
            adv.setText(R.string.Adv_Complex);
            adv.getResources().getColor(R.color.red);
        } else {
            x1 = (-b) / (2 * a);
            tex_formula = "$$" + aa + "x^2 + " + bb + "x + " + cc + " = 0" + "$$";
            adv.setText(R.string.Adv_Reals);
        }
        formula_with_user_inputs.setText(tex_formula);
        step_1.setText(R.string.Step_1);
        step_1_quation_demo.setText("$$" + "\\" + "Delta" + "\\" + " = " + "b" + "^2 - " + "4" + "\\" + "cdot" + " a" + "\\" + "cdot" + " c" + "$$");
        step_1_equation.setText(step_1_text);
        step_1_equation_2.setText(step_1_text_2);
        step_1_equation_3.setText(step_1_text_3);
        step_2.setText(R.string.Step_2);
        step_2_equation.setText("$$" + "x_{1,2} = " + "{" + "-b" + "\\" + "pm" + "\\" + "sqrt" + "{" + "b^2-4ac" + "}" + "\\" + "over" + "2a" + "}" + "." + "$$");
        step_2_equation_1.setText(step_2_text_1);
        step_2_equation_2.setText(step_2_text_2);
        step_2_equation_3.setText(step_2_text_3);
        step_2_equation_4.setText(step_2_text_4);
        solution_description.setText(R.string.Solution_is);
        solution.setText(solution_text);
    }
}
